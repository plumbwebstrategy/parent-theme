<?php
	
//add acf via PHP
if(function_exists('acf_add_local_field_group')):

	acf_add_local_field_group(array(
		'key' => 'group_5ae7d061b4bb7',
		'title' => 'Theme Options',
		'fields' => array(
			array(
				'key' => 'field_5b50f596f7969',
				'label' => 'Menus',
				'name' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'top',
				'endpoint' => 0,
			),
			array(
				'key' => 'field_5b50f59df796a',
				'label' => 'Hide Advanced Menus',
				'name' => 'hide_advanced_menus',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 1,
				'ui' => 1,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array(
				'key' => 'field_5b2170fbbebf1',
				'label' => 'Tracking Code',
				'name' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'top',
				'endpoint' => 0,
			),
			array(
				'key' => 'field_5b217104bebf2',
				'label' => 'Google Tag Manager - Header Code',
				'name' => 'google_tag_manager_header_code',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
			),
			array(
				'key' => 'field_5b21711fbebf3',
				'label' => 'Google Tag Manager - Body Code',
				'name' => 'google_tag_manager_body_code',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'theme-options',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
		'modified' => 1553918978,
	));

endif;

//disable acf editing on production and staging
function show_acf_menu_dev() {	
	
	if(!defined('ENV')) {
		return true;
	}
	
    if(ENV === 'Development') {
        return true;
    } else {
        return false;
	}
		
}
add_filter('acf/settings/show_admin', 'show_acf_menu_dev');


//check to see if there are any acf fields to sync
function check_acf_fields_sync($json_dirs) {
	
	$groups = acf_get_field_groups();
	
	if(empty($groups)) return;

	//find JSON field groups which have not yet been imported
	$sync = array();
	
	foreach($groups as $group) {
		
		$local 		= acf_maybe_get($group, 'local', false);
		$modified 	= acf_maybe_get($group, 'modified', 0);
		$private 	= acf_maybe_get($group, 'private', false);

		// ignore DB / PHP / private field groups
		if ($local !== 'json' || $private) {
			// do nothing
		} elseif(!$group['ID']) {
			$sync[$group['key']] = $group;
		} elseif($modified && $modified > get_post_modified_time('U', true, $group['ID'], true)) {
			$sync[$group['key']]  = $group;
		}
		
	}

	if(empty($sync)) {
		return;
	} else {
		add_action('admin_notices', 'add_acf_syn_admin_message');
	}
	
}
add_action('admin_init', function() {
		
	if(!class_exists('acf') || ENV !== 'Development') return false;

    $path = get_stylesheet_directory() . '/_assets/acf';
    $json_dirs = array($path);
	
    check_acf_fields_sync($json_dirs);
	
});

function add_acf_syn_admin_message() {
	
    ?>
    <div class="notice notice-error">
        <p><?php _e( 'You need to sync ACF!', 'sample-text-domain' ); ?></p>
    </div>
    <?php
	
}
