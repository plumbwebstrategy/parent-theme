<?php

//truncate string to within a certain character count and end seemlessly between words
function truncate_str($str, $character_count){
    return strtok(wordwrap($str, $character_count, "...\n"), "\n");
}
