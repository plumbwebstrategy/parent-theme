<?php

//replaces gravity forms spinner
function custom_gforms_spinner($src){
    return get_template_directory_uri() . '/_assets/img/loader.svg';
}
add_filter( 'gform_ajax_spinner_url', 'custom_gforms_spinner' );


//load gravity forms scripts in footer
add_filter('gform_init_scripts_footer', '__return_true');
