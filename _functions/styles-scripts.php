<?php

//removes emoji styles and scripts
remove_action('wp_head', 'print_emoji_detection_script', 7); 
remove_action('admin_print_scripts', 'print_emoji_detection_script'); 
remove_action('wp_print_styles', 'print_emoji_styles'); 
remove_action('admin_print_styles', 'print_emoji_styles');


//remove jQuery migrate
function dequeue_jquery_migrate(&$scripts){
	if(!is_admin()){
		$scripts->remove('jquery');
		$scripts->add('jquery', false, array('jquery-core'), '1.10.2');
	}
}
add_filter('wp_default_scripts', 'dequeue_jquery_migrate');


//remove WP embed for oEmbed
function my_deregister_scripts(){
	wp_deregister_script('wp-embed');
}
add_action('wp_footer', 'my_deregister_scripts');


//add custom javascript to admin
function plumb_admin_assets(){
	wp_enqueue_script('admin-js', get_template_directory_uri() . '/_assets/js/admin.min.js');
	wp_enqueue_style('admin-styles', get_template_directory_uri() . '/_assets/scss/admin.css');
}
add_action('admin_enqueue_scripts', 'plumb_admin_assets');
