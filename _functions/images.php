<?php

//disable WP from creating a max file size starting in 5.3
add_filter('big_image_size_threshold', '__return_false');

//add custom image sizes
add_image_size("full_bleed", 1599, 0, false);


//makes background image retina compatible at specified dimensions
function get_retina_background_image($image_id, $element, $breakpoints){
	
	$embed_styles = '';
		
	foreach($breakpoints as $breakpoint):
		
		$url = wp_get_attachment_image_src($image_id, $breakpoint[0])[0];
		$url_server_path = str_replace(get_site_url(null, '/', 'http'), $_SERVER['DOCUMENT_ROOT'] . '/', $url);
		$url_server_path = str_replace(get_site_url(null, '/', 'https'), $_SERVER['DOCUMENT_ROOT'] . '/', $url_server_path);
		
		//forces retina at this breakpoint (works well for using the full bleed retina version above ~1600px)
		if(array_key_exists(2, $breakpoint)){
			if($breakpoint[2]){
				$extension_pos = strrpos($url, '.');
				$url = substr($url, 0, $extension_pos) . '@2x' . substr($url, $extension_pos);
			}
		}
		
		if(!file_exists($url_server_path)) continue;
		
		$embed_styles .= "@media all and (min-width: {$breakpoint[1]}px){";
			
			$embed_styles .= "$element{ ";
			
				$embed_styles .= "background-image: url($url);";
				
			$embed_styles .=  "}";
			
		$embed_styles .= "}";

		
		if($breakpoint[0] == "full" || strpos($url, ".svg")) continue;
		
		//skip retina version since it was forced for all users
		if(array_key_exists(2, $breakpoint)){
			if($breakpoint[2]) continue;
		}
		
		$url = wp_get_attachment_image_src($image_id, $breakpoint[0])[0];
		$extension_pos = strrpos($url, '.'); 
		$retina_url = substr($url, 0, $extension_pos) . '@2x' . substr($url, $extension_pos);
		$retina_server_path = str_replace(get_site_url(null, '/', 'http'), $_SERVER['DOCUMENT_ROOT'] . '/', $retina_url);
		$retina_server_path = str_replace(get_site_url(null, '/', 'https'), $_SERVER['DOCUMENT_ROOT'] . '/', $retina_server_path);

		if(!file_exists($retina_server_path)) continue;
		
		$embed_styles .= "@media only screen and (-webkit-min-device-pixel-ratio: 2) and (min-width: {$breakpoint[1]}px),";
		$embed_styles .= "only screen and (min--moz-device-pixel-ratio: 2) and (min-width: {$breakpoint[1]}px),";
		$embed_styles .= "only screen and (-o-min-device-pixel-ratio: 2/1) and (min-width: {$breakpoint[1]}px),";
		$embed_styles .= "only screen and (min-device-pixel-ratio: 2) and (min-width: {$breakpoint[1]}px),";
		$embed_styles .= "only screen and (min-resolution: 192dpi) and (min-width: {$breakpoint[1]}px),";
		$embed_styles .= "only screen and (min-resolution: 2dppx) and (min-width: {$breakpoint[1]}px){";
			
			$embed_styles .= "$element{";
			
				$embed_styles .= "background-image: url($retina_url);";
				
			$embed_styles .= "}";
		
		$embed_styles .= "}";
	
	endforeach;
	
	return '<style>'.$embed_styles.'</style>';
	
}
