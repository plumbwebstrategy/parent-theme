<?php

//move Yoast plugin meta box to bottom of page
function set_yoast_priority() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'set_yoast_priority');


//removes Yoast plugin SEO columns
function admin_remove_columns( $columns ) {
	
	unset($columns['wpseo-score']);
	unset($columns['wpseo-title']);
	unset($columns['wpseo-metadesc']);
	unset($columns['wpseo-focuskw']);
	unset($columns['wpseo-score-readability']);
	unset($columns['wpseo-links']);
	unset($columns['wpseo-linked']);
	
	return $columns;
	
}
add_filter('manage_edit-post_columns', 'admin_remove_columns');
add_filter('manage_edit-page_columns', 'admin_remove_columns');


//wrap videos with div to make responsive
function wrap_embedded_video($html) {
    return "<div class='embed-container'>$html</div>";
}
add_filter('embed_oembed_html', 'wrap_embedded_video', 10, 3);
add_filter('video_embed_html', 'wrap_embedded_video');
