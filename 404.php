<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>

	<div id="not-found" class="wp-block-custom">
		
		<div class="container">
			
			<h1><?php echo __('Page Not Found', 'plumb'); ?></h1>
			
			<p><?php echo __("Sorry, this page could not be found. Please use the main menu to find what you're looking for.", 'plumb'); ?></p>
			
		</div>
		
	</div>

<?php get_footer(); ?>
