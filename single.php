<?php
/**
 * The Template for displaying all single posts.
 */

get_header(); ?>

	<?php the_content(); ?>

<?php get_footer(); ?>
