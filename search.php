<?php
/**
 * The template for displaying Search Results pages.
 */

get_header(); ?>
	
	<div id="search" class="wp-block-custom">
		
		<h1><?php echo __('Search', 'plumb') . ': ' . get_search_query(); ?></h1>
	
		<?php if(have_posts()): ?>
		
			<?php while(have_posts()) : the_post(); ?>
				
				<div class="search-result">
					
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt(); ?>
	
				</div>
	
			<?php endwhile; ?>
	
		<?php else: ?>
	
			<p><?php echo __('Sorry, nothing could be found for the search term:', 'plumb'); ?> '<?php echo get_search_query(); ?>'.</p>
	
		<?php endif; ?>
	
	</div>

<?php get_footer(); ?>
