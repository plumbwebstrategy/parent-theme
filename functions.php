<?php

require get_template_directory() . '/_functions/misc.php';
require get_template_directory() . '/_functions/acf.php';
require get_template_directory() . '/_functions/custom-post-tax.php';
require get_template_directory() . '/_functions/styles-scripts.php';
require get_template_directory() . '/_functions/admin.php';
require get_template_directory() . '/_functions/images.php';
require get_template_directory() . '/_functions/gravity-forms.php';
